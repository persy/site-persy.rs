# persy.rs

Website for [Persy](https://persy.rs).

Website code runs on [Cobalt](https://github.com/cobalt-org/), a static site generator.

## Run

Download a Cobalt release: https://github.com/cobalt-org/cobalt.rs/releases

Untar and copy it into (ex.) `~/.local/bin/`

$ cd site-persy.rs

$ cobalt serve
...
[info] Server Listening on http://localhost:3000
...
