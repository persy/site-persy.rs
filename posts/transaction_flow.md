---
layout: post.liquid

title: Transaction Flow
published_date: 2020-02-19 13:00:00 -0500
is_draft: true
---

Here is a (hope not too long) description for how the transactions are handled in Persy.


## Components

As first we need to list the components that can be involved in a transactions, that are almost
all the component of Persy.

- Journal
- Address
- Index
- Allocator
- Snapshots
- Transaction
- Root Pages

Each component has it's own role, let's see them:

Journal:

The journal just log all the operations happened in transaction, to the disc, being Persy a copy on write storage
need to keep only the metadata of the transaction no the actual data.

Address:

The address has the role of keeping track of all the records, and on the specific of the transaction
it does also manage the swap of the reference to the data of a record to the new version in case of update, 
and id generation in case of insert, also handle the locking and concurrency checks for the record.

Index:

The index structure is actually involved only if indexes are involved in the transaction, 
it does not have a active role in the transaction, it just keep locking logic for concurrent
create/drop of indexes keeping them coherent with other index operations.

Allocator:

The Allocator manage all the disk (persistent) memory related operations, so it handle the allocation/read/write/free of disk (persistent) memory

Transaction:

The transaction it actually keep in (non persistent) memory the state of the 'running' transaction.

Root Pages:

The root pages are the structures where a keep the metadata of all the previous structures (Journal, Allocator, Address),
this are the most critical part of the system, a wrong information in one of the metadata of this 3 structures, will not
allow you to read properly the database.

Snapshot :

The snapshot is a in memory (not persistent), structure, that keep track of old record values, to allow frozen in 
time read.


## Transaction Flow

Now let's go to the transaction flow,

### Start

The start of the transaction is pretty easy, a new log entry is insert in the journal and add some tracking of 
the new transaction in a list of active transaction to block the clean of the journal and avoid to remove transaction related
metadata of not yet committed transactions.

### Operations 
As soon as a transaction is started a set of operations can be done on it, till the prepare commit

Record Operations:

Record operations (CRUD) can happen on a transaction, if the operation do involve
some new data (insert&update) a free page is requested to the allocator and used to write the data directly, also in case of insert 
is requested to the Address structure a new identifier (PersyId) to be used for this record, all the meta information
of this operations are stored in memory in the Transaction structure and logged in the Journal for recover in case of crash.

Index Operations:

The index operations (put&get&remove) are just kept in (not persistent) memory, inside the transaction structure, for
the length of the transaction, till the prepare commit. 

Structural Operations:

The structural operations, create and drop of indexes and segments, are logged in the journal and for the create operations is done 
a allocation of a new SegmentId|IndexId using the address structure also some pages are pre-allocated for use to keep the references
to the records, the Ids and the pages than will need to be written in the root page of the address structure.


### Prepare Commit

At prepare commit, is where most of the operations happens, for each structure there are specific operations, the first applied
operations are Indexes operations because they rely on the underlying records structure for keep the data, and the record operations and
structure operations are applied.

Indexes:

for the indexes, each key operation is run trough the index structure, when the operations are applied to the index structure,
 are collected a set of record operations for the index structures, that will be then treated as the normal record operations,

Records:
for each record operation in the transaction, in order will be acquired a lock for changing the specific record and the version consistency check will be run if required, 
if a consistency check fail the transaction will be aborted and all the lock and resources released, if successful the locks will be hold till
the commit phase.

Structural:
for structural operations will be acquire locks to avoid concurrent structural operations on the same segment or index.

Snapshot:
When all the checks are finished, all the old record values(the pointer to the old page keeping the record content) are collected and used
to prepare a structure to handle snapshot read.

End of prepare commit:
Finished all the checks, will be logged a record that say the transaction reached the prepare commit successfully.
And then an fsync is run, or scheduled to run.


### Commit

At commit is where all the transaction data is made public and all the locked resources are released, the commit phase 
do not have any logic failure that cannot be retried or re-applied.

Address:
In the address will all the pointer to the record data will be swapped to a new page with the new data, after all the swap the locks are 
released.

Root Pages:
If any structural operation like a create drop of segment or index, is done, or in case of new record pages at the address the root pages
that keep this meta information are added to the flush queue, an additional fsync is also issued just to make sure root structure are 
flushed before the journal is cleaned.

Snapshot:
The snapshot reference count is count down.

Journal:
In the journal is logged that the transaction is committed successfully, so eventual crash recovery procedure will know that all the data
in the transaction need to be re-applied.

### Last Snapshot Release

This is step is not conceptually part of the transaction but when the reference count of a snapshot reach 0 most of cleanup is done and it 
may happen during the commit of the transaction or later on, in this step all the old pages contains the old information of the record will be made available for reuse, and 
the transaction id stored at the start of the transaction, is removed allowing the journal cleanup, 
At the end of this step a cleanup log is add to mark the overall complete of the transaction and that no recover
operations are needed to run for this specific transaction if found in the log.



 



