---
layout: post.liquid

title: Persy 0.9
published_date: 2020-03-02 00:00:30 +0100
categories: ["news"]
---



With slightly more time then previous release but still quick enough is coming out a new release, this bring a quite big set of features and fixes.

## What is new in Persy 0.8

Starting from the fixes, with the release of 0.8 was introduced the multi-thread support on indexes, this did actually had a few issue, bringing in the need of 3 hotfixes, that did not completely fix the problem,
at the end was needed some disc information change for completely fix the concurrency problem so a new minor release was needed, from my tests this new release fixes all the issue related to concurrency on indexes,
so from 0.9 is safe do do concurrent put and remove, in 0.8.x I would suggest not to have two concurrent prepare_commit with changes on the same index.  

Performance as well where improved from my tests even though more can be optimized, and all the basic performance should consider that` prepare_commit` before return the control will make sure that the `fsync` happened, so the number of transactions that can be done single thread are strictly 
dependent to the latency of the disc, if you want some numbers feel free to write down your own micro-benchmark, there is nothing better then your code on your machine to verify the performances you need.


In terms of new features two new set of API landed in this version:  

The first one is the [Snapshot API](https://docs.rs/persy/0.9.0/persy/struct.Snapshot.html), that allow to take a read snapshot and make sure that all the subsequent read on that snapshot are stopped at that specific point in the transactions flow,
ignoring all the subsequent committed transactions.

The second is a better [Recover API](https://docs.rs/persy/0.9.0/persy/struct.Recover.html) to identify and handle the state of the transactions on open after a crash, now is possible to list all the transactions with the relative state of each and mark them to be committed or rollback in case they are still in a prepared state.


Few more optimization in term of performances where done and bugs here and there where solved, this release define more or less what will be the general status of this project API, there may be some refinements on naming or some extensions or add of functionalities but not more new sets of APIs.

## Future Plans

For future works there are refinements in API, stabilization of disc structures, and all the optimization on data storing prior the 1.0.0 release that will define the first that will have backward disc compatibility, 
so I guess there will be still a 2 o 3 more minor before the 1.0.0.




