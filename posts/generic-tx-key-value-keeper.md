---
title: Generic Tx Key-Value Keeper
published_date: 2023-09-26 21:00:00 +0100
layout: post.liquid
categories: ["news"]
is_draft: true
---

Persy provide an API, that support multiple indexes at the same time, also the possibility 
to use multiple indexes in the same transaction, and have specific types for specific index that 
may be any of the supported index types.

This in a strongly typed language like Rust provide a challenge on how to keep the type and be type safe
and have a generic structure like the Persy Transaction that is not typed with the index types and cannot be typed
because of the multiple types and multiple combinations of types possible in the indexes.

To solve this problem originally I used a well know feature of rust that allow this, that are Enums, the Enums
allow to have multiple variant with different values of different type in the content of the variant, so was easy
to do an Enum and map all the possible types of index, in reality I have two types, one for the case of keys and the 
other for the case of values, but that is just a way I choose to implement it.

This works fine, but there is a drawback being the Transaction agnostic of the index types, that need to keep inside 
the values and than be able to apply them a commit time, I needed to do some mapping code that allowed to map all the 
possible combination of key/value types possible, and than execute the typed logic of the index, this mapping force 
to compile the index logic for each combination of types ahead of time, even if the end user do not really use this combination
of types, having persy 15 possible key types, all the combination produce 225 possible implementations of the indexes, all 
compiled ahead of time, this make the compilation of Persy really slow!!!!

To improve the compilation speed and allow me to add additional types in future without making the compilation time unbearable, 
I went to try the other other way to have multiple unknown types inside an instance without making them go up to the base type,
that is to use the dynamic types that means the type `std::any::Any`, with this I can ensure that the types that are compiled
are only the one that are used in the application, so speeding up a lot the compilation times of Persy.



This may have performance impact ?!?!
This is the compilation performance  improvement ?!?!?!



