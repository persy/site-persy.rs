---
layout: post.liquid

title: Persy 1.1
published_date:  2021-11-20 21:00:30 -0500
categories: ["news"]
---


Quickly after the release of Persy 1.0 just a bit less than 4 months we have the 1.1 this release does not have big changes
compared the previous release in terms of functionality or stability, most of the work that went in this release was just 
performance tuning.

## What's new in 1.1

The improvement in the performance happen in two main areas:

- Complex index types, namely ByteVec and String used to do a lot of allocation when index pages were loaded for get or put operations, I have done
a refactor on how index types are handled that allowed me to implement a couple of wrappers for these types that avoid allocations, 
this change improved the performance of fairly big indexes with this specific cases of a factor of 3 times in my custom tests.
The refactor did also restructure the internal index logic in a way that allow to introduce advanced features like custom ordering, 
but this is still far away in the feature.

- Removed not needed disk access when space disk was allocated, this speed up in all the cases of inserts and updates when
some space was recycled, this has had a fairly smaller impact in performance, around 5%, but cover a more wide spread
set of use cases.

In terms of other changes there were some more additional refactors and cleanups, the remove of the byteorder dependency in favour of std implementations
and pretty much that's it, since the release 1.0.x also I did not see major issue also feedbacks regards the stability were quite positive, I guess the 11 hotfix 
of the pre 1.0 releases covered a lot of cases already.

This release has API and disk compatibility with 1.0

## What's coming next.

Additional performance improvement are in work with the aim to introduce some optional weakened durability that will allow transaction commit with way lower latency, guaranteeing still no corruption in case
of crash.
 I'm still doing profiling all over the project to find additional places where improvements in terms of speed can be done.

