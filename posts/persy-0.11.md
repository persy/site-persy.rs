---
layout: post.liquid

title: Persy 0.11
published_date: 2020-07-10 00:00:30 +0100
categories: ["news"]
---

After a few months from the 0.10 here we are with a new release, this is mostly a stabilization and finalization release, let's go trough the details.

## What's new in Persy 0.11

### Fixes
All the possible leak in case of crash are now fixed, there are no more major know cases of disk leak.


### Features
#### Disk Compatibility Support
Disk compatibility has been implemented for all the persistent formats, anyway this is a complex problem and often even if
the data structures are thought in a way that could be evolved may not cover all the cases, only time can tell if what is done now will be enough, 
in any case the support is there and the files created with this version should be supported for all the range of future 0.X and 1.X releases.

#### Increase number of possible segments
Increased the maximum number of possible segments in a file from 65k (u16) to 4M (u32)

#### Improved  Disk trimming
Now the size of the file will decrease on delete all the time is possible, in the past versions the file was trimmed only if the 
memory freed at that moment was at the end of the file and only for the size of the specific memory freed, now the file will be trimmed for all the free memory at the end of the file.

#### Disk Usage Optimizations
In this version the disk format changed quite a lot to use more dynamic size formats, this allows to reduce the general disk use, and relatively fit more 
data in the in memory cache


## Next Step

After this release the plan is to release the 1.0, the only major changes left is to review the errors to have specific error for specific methods,
some additional testing and use in more real use cases is needed also, looking for get some feedbacks from early adopters in terms of stability. 
