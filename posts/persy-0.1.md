---
layout: post.liquid

title: Persy 0.1
published_date:  2017-04-29 21:00:30 -0500
categories: ["news"]
---

This is the announcement of the release of **Persy** the initial step for a single file storage engine fully written in Rust.

## What can be done with Persy 0.1

Presy 0.1 can be already used as a transactional storage of `Vec<u8>` inside a file, so as simple crash resistant data file format, or as a backend of a experimental database.


## What is ready in 0.1

- `Vec<u8>` based record insert,update,delete and read
- Records grouping based on segments
- Create and Drop of Segment
- Transactional Record and Segment operations
- Read-Committed transaction isolation
- Two-Phase commit 
- Consistency guaranteed by transaction log
- Non Transactional record scan
- Full Multithread support for record operations

## What is unstable in 0.1

- Unstable Segment operations in Multithread
- Missing record scan in transaction 
- Missing file lock for already open files
- API not yet finalized
- Missing public API for transaction finalize/rollback during transaction recover
- Missing addressing space reuse after record delete
- Suboptimal addressing space update


