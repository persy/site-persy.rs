---
layout: post.liquid

title: Persy 0.2
published_date: 2017-09-12 21:00:30 -0500
---

Persy is a storage based on a single file where data can stored in sub-containers called "segment", so as first step let's create a file the basic file add on it a segment and insert on it some raw data.

```rust

    //Create the file
    Persy::create("./storage.persy")?;
    //Open the file for operations
    let persy = Persy::open("./storage.persy",Config::new())?;
    //Start a transaction all the operations in persy are done inside a transaction.
    let mut tx = persy.begin()?;
    //Create a segment called "seg" using the started tx.
    persy.create_segment(&mut tx, "seg")?;
    //Prepere some raw data
    let data = vec![1;20];
    //Insert the data inside the segment with the current tx.
    let id = persy.insert_record(&mut tx, "seg", &data)?;
    //Commit the tx.
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

To notice that the transaction is a two phase transaction to allow to implement complex use cases.

*be aware that file locking is not present in persy 0.2 so there is no controll if two process try to access the same file*

Next step read scan the data that we just insert this can be done without a transaction

```rust 

   let id = None;
   let to_find = vec![1;20];
   for record in persy.scan_records("seg")? {
        //.... do something with the record.id and record.content
        if record.content == to_find {
            id = Some(record.id);
            break;
        }
    }

```

Update the record to another content
```rust 

    let update_id = //.... get from insert or from a scan.
    //Begin a transaction
    let mut tx = persy.begin()?;
    let new_data = vec![2;20];
    // Update the record with new data
    persy.update_record(&mut tx, "seg", &update_id, &new_data)?;
    //Commit the tx.
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

finally delete the record
```rust

    let delete_id = //.... get from insert or from a scan.
    //Begin a transaction
    let mut tx = persy.begin()?;
    // delete the record
    persy.delete_record(&mut tx, "seg", &delete_id)?;
    //Commit the tx.
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

For a full overview of the capabilities of persy, check the [docs](https://docs.rs/persy/0.2.0/persy/struct.Persy.html)



