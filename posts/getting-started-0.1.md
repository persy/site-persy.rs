---
layout: post.liquid

title: Getting Started 0.1
published_date: 2016-04-08 13:00:00 -0500
---


## Creating the first storage file and opening it

```rust

    Persy::create("./storage.persy")?;
    let persy = Persy::open("./storage.persy",Config::new())?;

```


## Segment creation, you need a segment for do any record operation

```rust

    let mut tx = persy.begin()?;
    persy.create_segment(&mut tx, "segment_name")?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

## Record creation 

```rust

    let mut tx = persy.begin()?;
    let data = vec![1;20];
    let id = persy.insert_record(&mut tx, "segment_name", &data)?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

## Record updated, you need a record identifier to update it

```rust

    let mut tx = persy.begin()?;
    let data = vec![2;20];
    persy.update_record(&mut tx1, "segment_name", &id, &data)?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

## Record delete, you need a record identifier to delete it

```rust

    let mut tx = persy.begin()?;
    persy.delete_record(&mut tx, "segment_name", &id)?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

## Simple read record in tx, you need a record identifier to read it.

```rust

    let mut tx = persy.begin()?;
    let read = persy.read_record_tx(&mut tx, "segment_name", &id)?;
    if let Some(content) = read {
         // do something with the content
    }
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

## Simple read record , you need a record identifier to read it.

```rust

    let read = persy.read_record("segment_name", &id)?;
    if let Some(content) = read {
         // do something with the content
    }

```

## Record scan, only supported outside the tx

```rust

    for x in persy.scan_records("segment_name")? {
        //....
    }

```

## Segment drop, it will remove all the record of the specified segment

```rust

    let mut tx = persy.begin()?;
    persy.drop_segment(&mut tx, "segment_name")?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```


## Obviusly multiple operations can be done in a transaction, 0 change example
```rust

    let mut tx = persy.begin()?;
    let data = vec![1;20];
    let id = persy.insert_record(&mut tx, "segment_name", &data)?;
    let data = vec![2;20];
    persy.update_record(&mut tx1, "segment_name", &id, &data)?;
    let read = persy.read_record_tx(&mut tx, "segment_name", &id)?;
    if let Some(content) = read {
         // do something with the content
    }
    persy.delete_record(&mut tx, "segment_name", &id)?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```


