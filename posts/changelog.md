---
layout: post.liquid

title: Change Log
published_date: 2019-02-05 13:00:00 -0500
---

## Persy 1.6
<span class="done">✓ </span> Add API for create snapshot from a transaction commit  

## Persy 1.5
<span class="done">✓ </span> Fixed rebalancing of index on big sequential deletes  
<span class="done">✓ </span> Introduced dynamic index key keeping that speed up compilation times   
<span class="done">✓ </span> Add new experimental APIs for debug inspect 

## Persy 1.4
<span class="done">✓ </span> Fixed snapshot release, that solve file oversize and long recover on open  
<span class="done">✓ </span> Add limits of max size for transaction id   
<span class="done">✓ </span> Fix recovery of free list, that correct wrong reads after crash    
<span class="done">✓ </span> Improved journal logging to handle rollback on recover    
<span class="done">✓ </span> Avoid that wrong data in journal make recover fail  
<span class="done">✓ </span> Add specific errors on prepare for concurrent create of indexes and segments   
<span class="done">✓ </span> Memory optimization of the recovery process    
<span class="done">✓ </span> Fixed range operations in transaction    

## Persy 1.3
<span class="done">✓ </span> Increased resilience in case of crash   
<span class="done">✓ </span> Add limits of max size for index key values sizes and record sizes   
<span class="done">✓ </span> Fix on transaction recover logic   

## Persy 1.2
<span class="done">✓ </span> Reduced latency on background sync transactions  
<span class="done">✓ </span> Minor improvement in serialization and index management     

## Persy 1.1
<span class="done">✓ </span> Speedups on index operation with ByteVec and String types   
<span class="done">✓ </span> Minor speedup in record insert and update     

## Persy 1.0
<span class="done">✓ </span> Review of error management, each function has now specific errors   
<span class="done">✓ </span> Simplified index lookup results     
<span class="done">✓ </span> Add time based eviction of cache entries     
<span class="done">✓ </span> Add `commit` and `one` "sugar methods" to reduce users boilerplate  

## Persy 0.11
<span class="done">✓ </span> Add some basic compression and optimization for disc usage     
<span class="done">✓ </span> Stabilization of disc structures     
<span class="done">✓ </span> Trim fill sizes in all possible cases     
<span class="done">✓ </span> Increased number of possible segments     
<span class="done">✓ </span> Fix all possible disk leak in case of crash     

## Persy 0.10
<span class="done">✓ </span> Support for only in memory storage  
<span class="done">✓ </span> Improved space re-use   
<span class="done">✓ </span> Add support for background disc sync  
<span class="done">✓ </span> Improved disc access parallelism on unix systems   
<span class="done">✓ </span> Bug fixes on recovery and snapshots   
<span class="done">✓ </span> General API review and stabilization   

## Persy 0.9
<span class="done">✓ </span> Make public the snapshot API  
<span class="done">✓ </span> Remove of API deprecated in previous version  
<span class="done">✓ </span> Evolve recover API for more complex use cases  
<span class="done">✓ </span> Tons of fixes in indexes API for range lookups and concurrency operations  

## Persy 0.8
<span class="done">✓ </span> Reduce the index locking from whole index to index page   
<span class="done">✓ </span> Introduce new operations API on transaction structures       
<span class="done">✓ </span> Made sure that only a single fsync is needed for commit a transaction   
<span class="done">✓ </span> Optimization in index operations to reduce number of clones   

## Persy 0.7
<span class="done">✓ </span> Increase cases where is possible to shrink file on page free   
<span class="done">✓ </span> Introduction of new OpenOptions API for better files management  
<span class="done">✓ </span> Improved error management now is possible to get the original error  
<span class="done">✓ </span> Add support for indexing of `Vec<u8>` types  
<span class="done">✓ </span> First support for SegmentId from operations API  

## Persy 0.6
<span class="done">✓ </span> Add support for double ended iterators on index range API  
<span class="done">✓ </span> Add access to underling transaction for transaction aware iterators, this allow modifications while iteration  
<span class="done">✓ </span> Add support from u128,i128,f32,f64 types in index implementation    
<span class="done">✓ </span> Open and create APIs now support all parameters that convert to Path, in the same way of the File API   
<span class="done">✓ </span> Add support of shrinking of the file if the page freed is the last of the file  
<span class="done">✓ </span> Optimization of disk writes when there are multiple changes for the same index in the same transaction    

## Persy 0.5
<span class="done">✓ </span> Internal introduction of snapshot for more parallel index access  
<span class="done">✓ </span> Range lookup API on the index  
<span class="done">✓ </span> Partial flush safety for base metadata structures  
<span class="done">✓ </span> List of existing segments  
<span class="done">✓ </span> List of existing indexes  
<span class="done">✓ </span> Redesigned API for segment scan  

## Persy 0.4

<span class="done">✓ </span> Indexing of simple values  
<span class="done">✓ </span> Multiple index modalities support  
<span class="done">✓ </span> General clean up and optimization following clippy suggestions  
<span class="done">✓ </span> Improved error management
<span class="done">✓ </span> Improved transaction API and safety

## Persy 0.3

<span class="done">✓ </span> Double open check with file lock  
<span class="done">✓ </span> Guarantee disk space reuse recycling address space   
<span class="done">✓ </span> API for drive transaction recover in case of crash  
<span class="done">✓ </span> Introduced recover from crash and advanced concurrency management examples  
<span class="done">✓ </span> Made sure that recover work in multiple crash scenario  

## Persy 0.2

<span class="done">✓ </span> Transactional scan  
<span class="done">✓ </span> Multithread support for segment create and drop operations  
<span class="done">✓ </span> Flush operations optimizations  
<span class="done">✓ </span> Multiversion concurrency management for record update  

## Persy 0.1

<span class="done">✓ </span> Basic architecture  
<span class="done">✓ </span> Transaction Journal   
<span class="done">✓ </span> Transactional insert, update, delete, read   
<span class="done">✓ </span> Non transactional read and scan   
<span class="done">✓ </span> Two phase commit transactions   
<span class="done">✓ </span> LRU Cache   
<span class="done">✓ </span> Multithread Support for insert, update, delete, read operations   
<span class="done">✓ </span> Transactional segment create and drop   




