---
layout: post.liquid

title: Tools
published_date: 2019-07-28 12:00:30 -0000
---

### Persy Export Import
A tool for export and import data from Persy storages, useful for cold backup and data migration across Persy versions  
[Source](https://gitlab.com/persy/persy_expimp/)  [Docs](https://docs.rs/persy_expimp/)  [Crates](https://crates.io/crates/persy_expimp)


