---
layout: post.liquid

title: Persy 1.0
published_date:  2021-08-27 21:00:30 -0500
categories: ["news"]
---

Finally 1.0 is out! It has been roughly 4 years from the 0.1 release and 5 years from the first commit, 
with a total ~700 git commits from 6 contributors, 11 minor releases, and 37 hotfixes !!!

Specifically, this release been around for 8 months since the previous one, which had a few issues resulting in a long series of hotfixes. Since
the last big set of fixes problems started to trim out, so I'm pretty confident the 1.0 is going to be pretty stable.


## What's changed in 1.0

The 1.0 has received an overall review of the public API, so it does have incompatibilities with previous versions, in detail:

#### Review of Errors
The biggest part of this release is the review of the error management: now each function or group of functions has a specific error type that will specify
the errors that can happen and allow easier management for each case. 
There will still be the possibility to pass forward the error in a "catch all" error type: you can find more 
details on the approach in this [blog post](http://www.tglman.com/posts/rust_lib_error_management.html).
#### Review of index result API 
Another important breaking change is the simplification of the index APIs, for an easier way to iterate the results of 
a lookup without need to access some detailed structures.
#### Naming conventions for public types
Renamed a set of enum types to better respect the Rust conventions.
#### New minor APIs
Add minor 'sugar' APIs `one` and `commit` to simplify APIs use.

## What 1.0 Persy do

Now that Persy 1.0 is out, it is a good time to recollect all the features, there are a few orthogonal features provided:

#### Data types
 - Segments: Allow storing multiple records of arbitrary `Vec<u8>`  
 - Indexes: Allow associating a key to a value or a list of values, support basic Rust types and Segments records ids

#### Reading
  - Segments: can be scanned or a specific record can be read by record id
  - Indexes: can be read with get or range lookups
  - Meta structures: is it possible to list the existing segments and indexes
  - Snapshots: is it possible to take a snapshot and do read operations ignoring any subsequent write operations

#### Transactional Writing
 - Multiple segment insert, update, and delete operations, together with index put operations can be included in a single transaction
 - Creation and drop of segments and indexes in transaction
 - Read committed isolation
 - Two phase commit transaction, for easy integration with other transactional systems
 - Transaction recovery on open after crash with selection of transaction to commit or rollback
 - Possibility to associate custom metadata at start of a transaction for easier management of recovery in case of crash

#### Data Files
  - All data and metadata of Persy instance are stored in a single file.
  - The file can be accessed only by a process at time.
  - The file will grow and shrink as needed.

All of this with support of multithread, for more code related feature look at the [getting started](/posts/getting-started.html)
and at the [API docs](https://www.docs.rs/persy/).


## Future 

From now on the pace of development of Persy will slow down a bit, mainly to let the dust settle, let the APIs solidify 
and get more downstream testing. Development will progress splitting the focus on two main directions.

First, the 1.x evolution will focus mainly on performance improvement and minor backward compatible additional APIs,
the scope is also to guarantee compatibility for all 1.x releases on disk format and API.

A second part will be starting to define a 2.x that will focus mainly on collecting all the major breaking changes 
that cannot be done in the 1.x, so most of the development will progress first in 1.x until possible, then a 2.0 branch will 
start when enough breaking changes are collected.

In terms of features of Persy, I'm definitely happy with the current state, I've no specific new features that
I would like to include. This also means that I will let users drive more the proposal for new features, to
do that I've created a [label](https://gitlab.com/tglman/persy/-/issues?label_name[]=Enhancement%20Proposal) for new feature proposal issues that can be used to propose, discuss and vote new features.
This will drive the future development of Persy, together with anything that will come to my mind in the future.

Feel free to contribute!
