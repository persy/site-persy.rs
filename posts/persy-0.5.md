---
layout: post.liquid

title: Persy 0.5
published_date: 2019-05-20 21:00:30 -0500
categories: ["news"]
---
With a faster peace Persy 0.5 is coming to light, it was supposed to be a smaller release than the previous one, but it turns out to include a good amount of changes, 
here is an overview of all the changes:  

The biggest change is an internal refactor of the logic for handling transaction commit, introducing the ability (internal only for now) to have multiple persistent snapshot of the same record,
this feature has been implemented mainly to allow to have indexes read concurrently to indexes write as soon and index lookup start will access the index tree of the start of the lookup even if the tree is changing concurrently.  

Another important change is the improvement on ensure that in most case of crash committed transaction can be recovered safely and that the storage remain consistent and not corrupted even in case of half flush of a structural pages ( [check the datils here ](/posts/disc-safety.html)) ,
for guarantee safety on all the cases some optimizations on disc space reuse has been turned off for now, it will return in future with better design.
The only remaining potential issue in term of disc safety is handling of the failing of the `sync_all()` operation on the disc, this is not a common problem on on-disc file systems, so we can say that Persy is safe as far as operate on a disc.  

As new features we have the index range lookup, the new API for list existing segments and indexes, and a reviewed API for scanning records.

Last not real feature, the code coverage now is run and published every build thanks to some help from a friend.

This is all for this release more will be worked on for the next release:

Again there are not huge plans for the next release, it should be small it's going to mainly include some improvement on the index range API, and most of the work would be around running some benchmark and do some internal refactor for improve performances,
obviously there will be some refinement around the overall APIs and some work will start on producing some external tool that may help to work with Persy.

