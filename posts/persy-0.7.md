---
layout: post.liquid

title: Persy 0.7
published_date: 2019-10-05 00:00:30 +0100
categories: ["news"]
---

In the last couple of months just right after the release of Persy 0.6 the development speeded up a bit, a few people started to play with persy 
and some downstream projects as well, first reporting few critical issue that produced the 3 hotfix 0.6.1,0.6.2,0.6.3 and then starting contributing back.

## What is new in Persy 0.7

So the 0.7 release Included a bunch of new changes, in internal behaviour and public API, as written in the roadmap 
I did some optimization to ensure trimming of the file when possible on space free even though not in all the case is still trimmed yet, 
there where other optimization to reduce the amount of disc sync needed for commit a transaction producing some speedup and as well some refactor on the way
page operations are logged for increase safety in case of crash.

In terms of public features a new contributor Erik Zscheile added the support of `Vec<u8>` as key or value of the index, increasing a lot the possibility to index values, as
well he refactor the error management to manage correctly the error kind, this change break a bit the compile time compatibility with previous version, but this is the correct way.
Erik also did some more changes on internal code to clean it up and make it a bit more compact.

Another new API change is the introduction of new `OpenOptions` that allow a more fine and rich API to control the open and creation of persy files, 
this was done by Clément Renault a new contributor, who as well did some more testing and reporting of critical issues

Also a big thank you to all the other peoples that contributed to Persy in all the small details sometimes even not visible, this allowed to ship out a new minor release quickly with really good improvements.

## Future Plans

There are no new big feature that I've plan to introduce any soon, it may come a bigger refactor on usability of transaction API that is being driven that the small community is forming around persy,
for sure there will be more work to improve the speed, now the index speed is definitely not great, due to some wide lock that include also a disc sync, this may take a bit to solve due the complexity of index locking,
but I hope it can be included in the next release or the one after.



