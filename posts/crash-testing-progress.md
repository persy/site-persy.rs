---
title: Crash Testing Progress
layout: post.liquid
published_date: 2024-07-24 18:00:30 +0100
categories: ["news"]
---

Has not been long since the last release, more or less 4 months, but I do want to give some updates
anyway. The work on Persy as now, is mainly around stabilization, there are a set of new features that
I would love to add, but crash testing and fixing relative discovery is taking the priority.

Speaking about crash testing most of the work recently has been around build a small test project, that run
few executable multiple times, that do some read&write operations with Persy, periodically hard killing these processes to check if they can recover successfully after kill, this already brought me to find few bugs in the 
last 1.5.x of which most of them are already fixed in 1.5.1 release.

More work will be done with testing, also trying to extend a bit the kind of use cases tested, to make sure
that durability and stability bug are detected and fixed.

If you want to dig trough what is about, here is the [repo](https://gitlab.com/persy/crash_tests/) of the crash test
and as well here is a [branch](https://gitlab.com/persy/persy/-/tree/WIP_tracing?ref_type=heads) that introduce some tracing 
logic in Persy itself, which I have used to trace issues from the crash test, also if you have a crash case where recovery fail, feel
free to add a new executable at the repo.

Other than that, not much work as been done on Persy, there is only one new feature in the development branch
that will be released with the next minor, and is about returning a snapshot at the exact state of the transaction
when the transaction is committed.

Because of not many features in the latest development version, and most work done on bug testing and fixing that will be release 
with at patch(hotfix) version, it will probably take more than 6 months from the last minor to get a new minor.

That's all for the updates, will probably post again for the next minor, I will give another update if that do not 
happen timely.
 
